<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () { return redirect()->route('report.home'); });

Route::prefix('department')->group(function () {

	Route::get('/', 'DepartmentController@index')->name('department.home');

	Route::post('/export', 'DepartmentController@store');

	Route::post('/update', 'DepartmentController@update');

});

Route::prefix('account')->group(function () {

	Route::get('/', 'AccountController@index')->name('account.home');

	Route::post('/create', 'AccountController@store');

	Route::post('/update', 'AccountController@update');
	
});

Route::prefix('undefined')->group(function () {

	Route::get('/', 'UndefinedController@index')->name('undefined.home');

	Route::get('/download', 'UndefinedController@download')->name('repfaglfsis.download');
	
});

Route::prefix('report')->group(function () {

	Route::get('/', 'ReportController@index')->name('report.home');

	Route::get('/download', 'ReportController@download')->name('report.download');
	
});