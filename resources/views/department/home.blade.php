@extends('layouts.remark.home')

@section('content')
<div id="department" class="page animsition">
	<!--
	<div class="page-header">
		<h1 class="page-title">Departments</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)">Home</a></li>
			<li class="active">Departments</li>
		</ol>
		<div class="page-header-actions">
			<button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip" data-original-title="Edit">
				<i class="icon md-edit" aria-hidden="true"></i>
			</button>
			<button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip" data-original-title="Refresh">
				<i class="icon md-refresh-alt" aria-hidden="true"></i>
			</button>
			<button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip" data-original-title="Setting">
				<i class="icon md-settings" aria-hidden="true"></i>
			</button>
		</div>
	</div>
	-->
    <div class="page-content container-fluid">
		<div class="row">
			<div class="col-sm-6">
				<!-- Panel Static Labels -->
				<div class="panel">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="icon fa-database grey-600 font-size-24 vertical-align-bottom margin-right-5"></i> Income Statement
							<span class="label label-success label-sm">MySQL</span>
						</h3>
					</div>
					<div class="panel-body container-fluid">
						<form class="page-search-form" role="search">
							<input type="hidden" name="connection" value="mysql">
							<div class="input-search input-search-dark" style="max-width: none; margin: 0px auto 25px;">
								<i class="input-search-icon md-search" aria-hidden="true"></i>
								<input type="text" class="form-control" id="inputSearch" name="search" placeholder="Search Department">
								<button type="button" class="input-search-close icon md-close" aria-label="Close"></button>
							</div>
						</form>
						@if (Request::query('connection') == 'mysql' && Request::has('search'))
							<h4 class="page-search-title">Search Results For "{{ Request::query('search') }}"</h4>
							<p class="page-search-count">About <span>@{{ warehouse.length }}</span> result ( <span>{{ $time }}</span> seconds)</p>
						@endif
          				<!-- <p class="page-search-count">About <span>1,370</span> result ( <span>0.13</span> seconds)</p> -->
          				<div class="table-responsive">
							<table class="table table-bordered table-striped" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 40px;"></th>
										<th>Description</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="whs in warehouse.data">
										<td>
											<button v-if="whs.is_changed" v-on:click="syncDepartment(whs.PK_mscWarehouse)" type="button" style="width: 23px; font-size: 20px; padding: 0px;" class="btn btn-pure btn-warning icon md-refresh-sync-alert" data-toggle="tooltip" data-original-title="Sync Department"></button>
										</td>
										<td>@{{ whs.description }}</td>
									</tr>
									<tr v-if="!warehouse.data.length">
										<td colspan="2"> &nbsp; </td>
									</tr>
								</tbody>
							</table>
						</div>
						<nav>
							<ul class="pager pager-round" style="margin-bottom: 0px;">
								<li v-bind:class="[ warehouse.prev_page_url ? '' : 'disabled', 'previous']"><a v-bind:href="warehouse.prev_page_url">Previous</a></li>
								<li v-if="warehouse.data.length > 0"><span style="border: none;">@{{ warehouse.current_page }} of @{{ warehouse.last_page }}</span></li>
								<li v-bind:class="[ warehouse.next_page_url ? '' : 'disabled', 'next']"><a v-bind:href="warehouse.next_page_url">Next</a></li>
							</ul>
						</nav>
					</div>
				</div>
				<!-- End Panel Static Labels -->
			</div>

			<div class="col-sm-6">
				<!-- Panel Floating Labels -->
				<div class="panel">
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="icon fa-database grey-600 font-size-24 vertical-align-bottom margin-right-5"></i> BizBox Hospital Information System
							<span class="label label-success label-sm">Microsoft® SQL Server® 2012</span>
						</h3>
					</div>
					<div class="panel-body container-fluid">
						<form class="page-search-form" role="search">
							<input type="hidden" name="connection" value="sqlsrv">
							<div class="input-search input-search-dark" style="max-width: none; margin: 0px auto 25px;">
								<i class="input-search-icon md-search" aria-hidden="true"></i>
								<input type="text" class="form-control" id="inputSearch" name="search" placeholder="Search Department">
								<button type="button" class="input-search-close icon md-close" aria-label="Close"></button>
							</div>
						</form>
						@if (Request::query('connection') == 'sqlsrv' && Request::has('search'))
							<h4 class="page-search-title">Search Results For "{{ Request::query('search') }}"</h4>
							<p class="page-search-count">About <span>@{{ sqlsrv_warehouse.length }}</span> result ( <span>{{ $time }}</span> seconds)</p>
						@endif
						<div class="table-responsive">
							<table class="table table-bordered table-striped" style="margin-bottom: 0;">
								<thead>
									<tr>
										<th style="width: 40px;"></th>
										<th>Description</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="sqlsrv_whs in sqlsrv_warehouse.data">
										<td>
											<button v-on:click="exportDepartment(sqlsrv_whs.PK_mscWarehouse)" type="button" style="width: 23px; font-size: 20px; padding: 0px;" class="btn btn-pure btn-default icon md-mail-reply grey-600" data-toggle="tooltip" data-original-title="Export to MySQL"></button>
										</td>
										<td>@{{ sqlsrv_whs.description }}</td>
									</tr>
									<tr v-if="!sqlsrv_warehouse.data.length">
										<td colspan="2"> &nbsp; </td>
									</tr>
								</tbody>
							</table>
						</div>
						<nav>
							<ul class="pager pager-round" style="margin-bottom: 0px;">
								<li v-bind:class="[ sqlsrv_warehouse.prev_page_url ? '' : 'disabled', 'previous']"><a v-bind:href="sqlsrv_warehouse.prev_page_url">Previous</a></li>
								<li v-if="sqlsrv_warehouse.data.length > 0"><span style="border: none;">@{{ sqlsrv_warehouse.current_page }} of @{{ sqlsrv_warehouse.last_page }}</span></li>
								<li v-bind:class="[ sqlsrv_warehouse.next_page_url ? '' : 'disabled', 'next']"><a v-bind:href="sqlsrv_warehouse.next_page_url">Next</a></li>
							</ul>
						</nav>
					</div>
				</div>
				<!-- End Panel Floating Labels -->
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	var department = new Vue({
		el: '#department',
		data: function () {

			let token = $('meta[name="csrf-token"]').attr('content');

			let warehouse = {!! json_encode($warehouse) !!};

			let sqlsrv_warehouse = {!! json_encode($sqlsrv_warehouse) !!};

			return {
				token: token,
				warehouse: warehouse,
				sqlsrv_warehouse: sqlsrv_warehouse,
			}

		},
		methods: {
			/** Call this function to remove tooltip
			removeTooltip: function() {
				
				// Get element that have aria-describedby
				var e = this.$el.querySelector('[aria-describedby]');

				// Get tooltip id
				var tooltip = $(e).attr('aria-describedby');

				// Remove tooltip element
				$('#'.concat(tooltip)).remove();

			},
			**/
			syncDepartment: function (PK_mscWarehouse) {
				
				$.post("department/update", { 
					
					_token: this.token, PK_mscWarehouse: PK_mscWarehouse,

				}, function( data ){

					// Reload the current page, without using the cache
					window.location.reload(true);

				});

			},
			exportDepartment: function (PK_mscWarehouse) {

				$.post("department/export", { 
					
					_token: this.token, PK_mscWarehouse: PK_mscWarehouse,

				}, function( data ){

					// Reload the current page, without using the cache
					window.location.reload(true);

				});

			},
		},
		created: function () {  }
	});
</script>
@endsection