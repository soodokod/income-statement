@extends('layouts.remark.home')

@section('content')
<div class="page animsition">
	
	<div class="page-header">
		<!--
		<h1 class="page-title">Accounts</h1>
		 <ol class="breadcrumb">
			<li><a href="javascript:void(0)">Home</a></li>
			<li class="active">Departments</li>
		</ol>
		-->
		<div class="page-header-actions">
			<!--
			<button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip" data-original-title="Edit">
				<i class="icon md-edit" aria-hidden="true"></i>
			</button>
			<button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip" data-original-title="Refresh">
				<i class="icon md-refresh-alt" aria-hidden="true"></i>
			</button>
			<button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip" data-original-title="Setting">
				<i class="icon md-settings" aria-hidden="true"></i>
			</button>
			-->
			<a class="btn btn-sm btn-primary btn-round" href="{{ route('report.download') }}">
				<i class="icon md-download" aria-hidden="true"></i>
				<span class="hidden-xs">Excel (.xlsx)</span>
			</a>
		</div> 
	</div>
	
    <div class="page-content container-fluid">
		<div class="panel">
			<!-- 
			<header class="panel-heading">
				<h3 class="panel-title">
					X-Editable
					<span class="panel-desc">Click to edit.</span>
				</h3>
			</header> 
			-->
			<div class="panel-body">
				<!-- <table class="tablesaw table-striped table-bordered table-hover" data-tablesaw-mode="swipe" data-tablesaw-sortable data-tablesaw-sortable-switch data-tablesaw-minimap data-tablesaw-mode-switch> -->
				<table class="tablesaw table-striped table-bordered table-hover" data-tablesaw-mode="swipe" data-tablesaw-minimap>
					<thead>
						<tr>
							<!--
							<th data-tablesaw-sortable-col data-tablesaw-sortable-default-col data-tablesaw-priority="persist">Movie Title</th>
							<th data-tablesaw-sortable-col data-tablesaw-priority="3">Rank</th>
							<th data-tablesaw-sortable-col data-tablesaw-priority="2">Year</th>
							<th data-tablesaw-sortable-col data-tablesaw-priority="1">
								<abbr title="Rotten Tomato Rating">Rating</abbr>
							</th>
							<th data-tablesaw-sortable-col data-tablesaw-priority="4">Reviews</th>
							-->							
							@for ($i = 0; $i < count($sprepfaglfsis_thead); $i++)
							    <th @if($sprepfaglfsis_thead[$i] == 'Description') data-tablesaw-priority="persist" @else data-tablesaw-priority="{{ $i }}" @endif>{{ $sprepfaglfsis_thead[$i] }}</th>
							@endfor
						</tr>
					</thead>
					<tbody>
						@foreach ($sprepfaglfsis_tbody as $tr)
						<tr>
							<!--
							<td><a href="javascript:void(0)">Gone with the Wind</a></td>
							<td>1</td>
							<td>2015</td>
							<td>97%</td>
							<td>38</td>
							-->
							@foreach ($tr as $td)
							<td>{{ $td }}</td>
							@endforeach
						</tr>
						@endforeach
					</tbody>
              	</table>
              	<nav>
					<ul class="pager pager-round" style="margin-bottom: 0px;">
						<li class="previous @if (empty($sprepfaglfsis_tbody->previousPageUrl())) disabled @endif"><a href="{{ $sprepfaglfsis_tbody->previousPageUrl() }}">Previous</a></li>
						@if ($sprepfaglfsis_tbody->total() > 0)
							<li><span style="border: none;">Showing {{ $sprepfaglfsis_tbody->firstItem() }} to {{ $sprepfaglfsis_tbody->lastItem() }} of {{ $sprepfaglfsis_tbody->total() }} accounts</span></li>
						@endif
						<li class="next @if (empty($sprepfaglfsis_tbody->nextPageUrl())) disabled @endif"><a href="{{ $sprepfaglfsis_tbody->nextPageUrl() }}">Next</a></li>
					</ul>
				</nav>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')

@endsection