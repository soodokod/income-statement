@extends('layouts.remark.home')

@section('content')
<div class="page animsition">
	
	<div class="page-header">
		<!-- 
		<h1 class="page-title">Departments</h1>
		<ol class="breadcrumb">
			<li><a href="javascript:void(0)">Home</a></li>
			<li class="active">Departments</li>
		</ol> 
		-->
		<div class="page-header-actions">
			<!-- 
			<button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip" data-original-title="Edit">
				<i class="icon md-edit" aria-hidden="true"></i>
			</button>
			<button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip" data-original-title="Refresh">
				<i class="icon md-refresh-alt" aria-hidden="true"></i>
			</button>
			<button type="button" class="btn btn-sm btn-icon btn-primary btn-round" data-toggle="tooltip" data-original-title="Setting">
				<i class="icon md-settings" aria-hidden="true"></i>
			</button>
			-->
			<a class="btn btn-sm btn-primary btn-round" href="{{ route('repfaglfsis.download') }}">
				<i class="icon md-download" aria-hidden="true"></i>
				<span class="hidden-xs">repfaGLFSIS</span>
			</a>
		</div>
	</div>
	
    <div class="page-content container-fluid">
		<div class="panel">
			<!-- 
			<header class="panel-heading">
				<h3 class="panel-title">
					X-Editable
					<span class="panel-desc">Click to edit.</span>
				</h3>
			</header> 
			-->
			<div class="panel-body">
				@if($errors->has('repfaglfsis'))
				<div role="alert" class="alert alert-social alert-facebook alert-dismissible">
					<button aria-label="Close" data-dismiss="alert" class="close" type="button">
						<span aria-hidden="true">×</span>
					</button>
					<i class="icon md-alert-polygon" aria-hidden="true"></i>
					<h4>{{ $errors->all()['title'] }}</h4>
					<p>{!! $errors->all()['message'] !!}</p>
				</div>
				@endif
				<!-- <button class="btn btn-primary margin-bottom-10" id="editableEnable" type="button">enable / disable</button> -->
				<form class="page-search-form" role="search">
					<div class="input-search input-search-dark" style="max-width: none; margin: 0px auto 25px;">
						<i class="input-search-icon md-search" aria-hidden="true"></i>
						<input type="text" class="form-control" id="inputSearch" name="search" placeholder="Search Account">
						<button type="button" class="input-search-close icon md-close" aria-label="Close"></button>
					</div>
				</form>
				@if (Request::has('search'))
					<h4 class="page-search-title">Search Results For "{{ Request::query('search') }}"</h4>
				@endif
				<div class="table-responsive">
					<table class="table table-bordered table-striped" style="margin-bottom: 0;">
						<thead>
							<tr>
								<th>Description</th>
								<th>Department</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($repfaglfsis as $account)
	    					<tr>
								<td>{{ $account->TempAcctDesc }}</td>
								<td>
									<a class="account" href="javascript:void(0)" data-type="select" data-name="{{ $account->TempAcctDesc }}" data-pk="0" data-title="Select Department"></a>
								</td>
							</tr>
							@endforeach

							@if (!$repfaglfsis->count())
							<tr>
								<td colspan="2"> &nbsp; </td>
							</tr>
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	!function(document,window,$){
		
		"use strict";

		var Site=window.Site;
		
		$(document).ready(function($){
			
			Site.run();

			// https://vitalets.github.io/x-editable/docs.html
			var init_x_editable=function(){

				var warehouses = {!! json_encode($warehouses) !!};

				$.fn.editableform.buttons='<button type="submit" class="btn btn-primary btn-sm editable-submit"><i class="icon md-check" aria-hidden="true"></i></button><button type="button" class="btn btn-default btn-sm editable-cancel"><i class="icon md-close" aria-hidden="true"></i></button>';

				$.fn.editabletypes.datefield.defaults.inputclass="form-control input-sm";

				$.fn.editable.defaults.url="/account/create";

				$(".account").editable({
					
					emptytext: 'Undefined',

					source: warehouses,
					
					params: function(params){
						
						params._token = $('meta[name="csrf-token"]').attr('content');

						return params;

					},
					
					display: function(value,sourceData){
						// var colors={"":"gray", 1:"green", 2:"blue"}, elem = $.grep(sourceData, function(o){ return o.value === value; });
						// elem.length ? $(this).text(elem[0].text).css("color", colors[value]) : $(this).empty();

						if (value) {

							// Finds the elements of an array which satisfy a filter function
							var e = $.grep(sourceData, function(obj){ return obj.value == value; });

							$(this).text(e[0].text);	

						}

					},
					
					success: function(response, newValue) {
						
					    response.newValue = newValue;
					    
					    return response;

					}

				});

			};

			$.fn.editable.defaults.mode="inline";

			init_x_editable();

		});

	}(document,window,jQuery);
</script>
@endsection