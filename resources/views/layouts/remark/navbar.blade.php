<nav class="site-navbar navbar navbar-inverse navbar-fixed-top navbar-mega" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided" data-toggle="menubar">
            <span class="sr-only">Toggle navigation</span>
            <span class="hamburger-bar"></span>
        </button>
        <a class="navbar-brand navbar-brand-center" href="javascript:void(0)">
            <!-- <img class="navbar-brand-logo navbar-brand-logo-normal" src="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/images/logo.png') }}" title="Remark"> -->
            <!-- <img class="navbar-brand-logo navbar-brand-logo-special" src="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/images/logo-blue.png') }}" title="Remark"> -->
            <!-- <span class="navbar-brand-text hidden-xs"> {{ config('app.name') }} </span> -->
            <span class="navbar-brand-logo navbar-brand-logo-normal"> {{ config('app.name') }} </span>
        </a>
    </div>

    <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
            <!-- Navbar Toolbar -->
            <ul class="nav navbar-toolbar">
                <li class="hidden-float" id="toggleMenubar">
                    <a data-toggle="menubar" href="#" role="button">
                        <i class="icon hamburger hamburger-arrow-left">
                            <span class="sr-only">Toggle menubar</span>
                            <span class="hamburger-bar"></span>
                        </i>
                    </a>
                </li>
            </ul>
            <!-- End Navbar Toolbar -->
        </div>
        <!-- End Navbar Collapse -->
    </div>
</nav>

<div class="site-menubar">
    <div class="site-menubar-body">
        <div>
            <div>
                <ul class="site-menu">
                <!-- <li class="site-menu-category">General</li> -->
                    <li class="site-menu-item has-sub @if (Request::is('report')) active @endif">
                        <a href="{{ route('report.home') }}" data-dropdown-toggle="false">
                            <i class="site-menu-icon md-print" aria-hidden="true"></i>
                            <span class="site-menu-title">Report</span>
                            <!-- <span class="site-menu-arrow"></span> -->
                        </a>
                    </li>
                    <li class="site-menu-item has-sub @if (Request::is('undefined')) active @endif">
                        <a href="{{ route('undefined.home') }}" data-dropdown-toggle="false">
                            <i class="site-menu-icon md-help" aria-hidden="true">
                                @if ($badges->undefined > 0)
                                    <span class="badge badge-danger up" style="font-family: Roboto,sans-serif;">{{ $badges->undefined }}</span>
                                @endif
                            </i>
                            <span class="site-menu-title">Undefined</span>
                            <!-- <span class="site-menu-arrow"></span> -->
                        </a>
                    </li>
                    <!-- <li class="site-menu-category">Elements</li> -->
                    <li class="site-menu-item has-sub @if (Request::is('account')) active @endif">
                        <a href="{{ route('account.home') }}" data-dropdown-toggle="false">
                            <i class="site-menu-icon md-equalizer" aria-hidden="true"></i>
                            <span class="site-menu-title">Accounts</span>
                            <!-- <span class="site-menu-arrow"></span> -->
                        </a>
                    </li>
                    <li class="site-menu-item has-sub @if (Request::is('department')) active @endif">
                        <a href="{{ route('department.home') }}" data-dropdown-toggle="false">
                            <i class="site-menu-icon md-home" aria-hidden="true">
                                @if ($badges->departments > 0)
                                    <span class="badge badge-danger up" style="font-family: Roboto,sans-serif;">{{ $badges->departments }}</span>
                                @endif
                            </i>
                            <span class="site-menu-title">Departments</span>
                            <!-- <span class="site-menu-arrow"></span> -->
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>