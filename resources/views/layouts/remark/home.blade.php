<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta name="description" content="bootstrap material admin template">
  <meta name="author" content="">

  <title>{{ config('app.name') }}</title>

  <!-- X-CSRF-TOKEN -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  
  <!-- 
  <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="assets/images/favicon.ico">
  -->

  <!-- Stylesheets -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/css/bootstrap.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/css/bootstrap-extend.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/css/site.min3f0d.css?v2.2.0') }}">

  <!-- Skin tools (demo site only) 
  <link rel="stylesheet" href="../global/css/skintools.min3f0d.css?v2.2.0">
  <script src="assets/js/sections/skintools.min.js"></script>
  -->

  <!-- Plugins -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/animsition/animsition.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/asscrollable/asScrollable.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/switchery/switchery.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/intro-js/introjs.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/slidepanel/slidePanel.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/flag-icon-css/flag-icon.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/waves/waves.min3f0d.css?v2.2.0') }}">

  <!-- Plugins UIKit\Icons\Font Awesome -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/asrange/asRange.min3f0d.css?v2.2.0') }}">

  <!-- Plugins UIKit\Tooltip & Popover -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/webui-popover/webui-popover.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/toolbar/toolbar.min3f0d.css?v2.2.0') }}">

  <!-- Plugins UIKit\Buttons -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/ladda-bootstrap/ladda.min3f0d.css?v2.2.0') }}">

  <!-- Plugins Form\Editable -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/x-editable/x-editable.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/x-editable/address.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/typeahead-js/typeahead.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/select2/select2.min3f0d.css?v2.2.0') }}">

  <!-- Plugins Tables\Responsive -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/filament-tablesaw/tablesaw.min3f0d.css?v2.2.0') }}">

  <!-- Page UIKit\Icons\Font Awesome -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/examples/css/uikit/icon.min3f0d.css?v2.2.0') }}">

  <!-- Page UIKit\Tooltip & Popover -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/examples/css/uikit/tooltip-popover.min3f0d.css?v2.2.0') }}">

  <!-- Page UIKit\Buttons -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/examples/css/uikit/buttons.min3f0d.css?v2.2.0') }}">

  <!-- Page Structure\Pagination -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/examples/css/structure/pagination.min3f0d.css?v2.2.0') }}">

  <!-- Page Basic UI\Badges & Labels -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/examples/css/uikit/badges-labels.min3f0d.css?v2.2.0') }}">

  <!-- Page Structure\Alerts-->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/examples/css/structure/alerts.min3f0d.css?v2.2.0') }}">

  <!-- Fonts -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/fonts/material-design/material-design.min3f0d.css?v2.2.0') }}">
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/fonts/brand-icons/brand-icons.min3f0d.css?v2.2.0') }}">

  <!-- Fonts UIKit\Icons\Font Awesome -->
  <link rel="stylesheet" href="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/fonts/font-awesome/font-awesome.min3f0d.css?v2.2.0') }}">

  <!-- <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,700'> -->


  <!--[if lt IE 9]>
    <script src="../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->

  <!--[if lt IE 10]>
    <script src="../global/vendor/media-match/media.match.min.js"></script>
    <script src="../global/vendor/respond/respond.min.js"></script>
    <![endif]-->

  <style type="text/css"> .list-group-dividered .list-group-item:last-child { border-bottom-color: transparent; } </style>
  
  <!-- Scripts -->
  <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/modernizr/modernizr.min.js') }}"></script>
  <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/breakpoints/breakpoints.min.js') }}"></script>
  <script>
    Breakpoints();
  </script>
</head>
<body class="site-navbar-small">
  <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    @include('layouts.remark.navbar')
    
    @yield('content')
    
    <!-- Core  -->
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/animsition/animsition.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/asscroll/jquery-asScroll.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/mousewheel/jquery.mousewheel.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/asscrollable/jquery.asScrollable.all.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/ashoverscroll/jquery-asHoverScroll.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/waves/waves.min.js') }}"></script>

    <!-- Plugins -->
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/switchery/switchery.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/intro-js/intro.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/screenfull/screenfull.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/slidepanel/jquery-slidePanel.min.js') }}"></script>

    <!-- Plugins UIKit\Tooltip & Popover -->
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/webui-popover/jquery.webui-popover.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/toolbar/jquery.toolbar.min.js') }}"></script>

    <!-- Plugins UIKit\Buttons -->
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/ladda-bootstrap/spin.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/ladda-bootstrap/ladda.min.js') }}"></script>

    <!-- Plugins Form\Editable -->
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/x-editable/bootstrap-editable.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/typeahead-js/bloodhound.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/typeahead-js/typeahead.jquery.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/x-editable/address.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/moment/moment.min.js') }}"></script>

    <!-- Plugins Structure\Pagination -->
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/aspaginator/jquery.asPaginator.min.js') }}"></script>

    <!-- Plugins Tables\Responsive -->
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/filament-tablesaw/tablesaw.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/vendor/filament-tablesaw/tablesaw-init.js') }}"></script>

    <!-- Scripts -->
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/core.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/js/site.min.js') }}"></script>

    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/js/sections/menu.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/js/sections/menubar.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/js/sections/sidebar.min.js') }}"></script>

    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/configs/config-colors.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/js/configs/config-tour.min.js') }}"></script>

    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/components/asscrollable.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/components/animsition.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/components/slidepanel.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/components/switchery.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/components/tabs.min.js') }}"></script>

    <!-- Scripts UIKit\Tooltip & Popover -->
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/components/webui-popover.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/components/toolbar.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/examples/js/uikit/tooltip-popover.min.js') }}"></script>
    
    <!-- Scripts UIKit\Buttons -->
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/components/buttons.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/components/ladda-bootstrap.min.js') }}"></script>
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/examples/js/uikit/button.min.js') }}"></script>

    <!-- Scripts Form\Editable -->
    <!-- <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/assets/examples/js/forms/editable.min.js') }}"></script> -->

    <!-- Scripts Structure\Pagination -->
    <script src="{{ asset('library/getbootstrapadmin.com/remark/topicon/global/js/components/aspaginator.min.js') }}"></script>

    <!-- Vue.js: Progressive JavaScript Framework -->
    <script src="{{ asset('library/vuejs.org/js/vue.js') }}"></script>

    @yield('script')

    <!-- Google Analytics 
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../../../../www.google-analytics.com/analytics.js',
        'ga');

        ga('create', 'UA-65522665-1', 'auto');
        ga('send', 'pageview');
    </script>
    -->
  </body>
</html>