<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Schema;
use DB;

class UndefinedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];

        $repfaglfsis = DB::table('repfaglfsis as a')
            ->selectRaw('TRIM(a.TempAcctDesc) as TempAcctDesc, b.description')
            ->leftJoin('tempacctdesc as b', DB::raw('TRIM(a.TempAcctDesc)'), '=', 'b.description')
            ->whereNotIn('a.TempAccountType', ['D','E'])
            ->whereNotIn('a.TempClassType', ['S','H','T'])
            ->where('b.description', '=', null);

        // Config Search
        if ($request->has('search')) {

            $search = $request->input('search');

            $repfaglfsis->where(DB::raw('TRIM(a.TempAcctDesc)'), 'like', "%$search%");

        }

        $data['repfaglfsis'] = $repfaglfsis->get();

        $warehouses = DB::table('mscwarehouse')
            ->select('PK_mscWarehouse as value', 'description as text')
            ->get();

        $data['warehouses'] = $warehouses;

        return view('undefined.home', $data);
    }

    /**
     * Download repfaGLFSIS_<Hostname>
     */
    public function download()
    {
        $pattern = "/[\s-]+/";
        
        $replacement = "";

        $subject = gethostbyaddr($_SERVER['REMOTE_ADDR']);

        /** 
         * In CentOS 7 gethostbyaddr($_SERVER['REMOTE_ADDR']) does not return the host name, instead returns the IP address
         * 
         * [Solution: For CentOS 7 only]
         *
         *     $output = shell_exec("nmblookup -A $subject | head -n 2 | tail -n 1");
         *
         *     $subject = explode(' ', trim($output))[0]; 
         */

        $output = shell_exec("nmblookup -A $subject | head -n 2 | tail -n 1");

        $subject = explode(' ', trim($output))[0];

        //Search and Replace dashes or whitespaces
        $hostname = preg_replace($pattern, $replacement, $subject);

        $table = 'repfaGLFSIS_' . $hostname;

        $connection = 'sqlsrv_bizbox';

        $redirect = redirect()->route('undefined.home');

        // Checking for existence of table
        if (Schema::connection($connection)->hasTable($table)) {

            DB::table('repfaglfsis')->truncate();

            $repfaglfsis = DB::connection($connection)
                ->table($table)
                ->select("PK_$table as PK_repfaGLFSIS",
                    'FK_faGLFSReports','faGLAcct','faGLAcctDesc','TempAcctClass','TempAcctDesc',
                    'TempAccountType','TempClassType','tabindent','acctformula','glmonth','glyear',
                    'debit','credit','amount','ytodamount','ipdamount','opdamount')
                ->get();

            /**
             * Fix: Object of class stdClass could not be converted to string
             *      json_decode - If TRUE, returned objects will be converted into associative arrays.
             */
            $repfaglfsis = json_decode($repfaglfsis, true);

            DB::table('repfaglfsis')->insert($repfaglfsis);

        } else {

            $redirect->withErrors([
                'repfaglfsis' => [
                    'title'   => "Table '$table' doesn't exist",
                    'message' => "Kindly open (FMS) <i class='icon md-file-text' aria-hidden=true'></i> Report Manager &#8594; Income Statement, before downloading the latest repfaGLFSIS"
                ]
            ]);

        }

        return $redirect;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
