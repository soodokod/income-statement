<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\SQLSRVWarehouse;
use App\Warehouse;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];

        $time_start = microtime(true); // Get current Unix timestamp with microseconds

        /** Config MySQL Warehouse **/

        $warehouse = Warehouse::select('PK_mscWarehouse', 'description')
            ->orderBy('description', 'asc');

        /** Config SQL Server Warehouse **/

        // Not included in Department
        $coll = collect([
            1000, // Not Applicable
            1014, // Dept.Ancill.Ware 001
            1029, // Dept.Ancill.Ware 002
            1032, // Dept.Ancill.Ware 003
            1036  // Dept.Ancill.Ware 004
        ]);

        $keys = Warehouse::select('PK_mscWarehouse', 'description')
            ->orderBy('description', 'asc')
            ->get()
            ->pluck('PK_mscWarehouse');

        $keys = $coll->merge($keys);
        
        $sqlsrv_warehouse = SQLSRVWarehouse::select('PK_mscWarehouse', 'description')
            ->whereNotIn('PK_mscWarehouse', $keys)
            ->orderBy('description', 'asc');

        // Config Search
        if ($request->has('search')) {

            $search = $request->input('search');

            $connection = $request->input('connection');

            switch ($connection) {

                case 'mysql':

                    $warehouse->where('description', 'like', "%$search%");

                    break;

                case 'sqlsrv':
                    
                    $sqlsrv_warehouse->where('description', 'like', "%$search%");

                    break;
                
                default: break;
            }
        }

        // Count Changed Warehouses and Unexported Department
        $whs = $warehouse->get();

        $whs = $whs->where('is_changed', true);

        $sqlsrv_whs = $sqlsrv_warehouse->get();

        $data['badge'] = $whs->count() + $sqlsrv_whs->count();

        // Details of LengthAwarePaginator [->paginate(25, ["*"], "mysql | sqlsrv")] https://github.com/laravel/framework/issues/18342

        $data['warehouse'] = $warehouse->paginate(25, ["*"], "mysql"); // MySQL

        $data['sqlsrv_warehouse'] = $sqlsrv_warehouse->paginate(25, ["*"], "sqlsrv"); // SQL Server
        
        $time_end = microtime(true); // Get current Unix timestamp with microseconds
        
        // Push time to $data
        $data['time'] = $time_end - $time_start;

        return view('department.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];

        DB::beginTransaction();

        try{

            // Validation
            $this->validate($request, [
                'PK_mscWarehouse' => 'required',
            ]);

            $PK_mscWarehouse = $request->PK_mscWarehouse;

            $sqlsrv_warehouse = SQLSRVWarehouse::select('PK_mscWarehouse', 'description')
                ->where([
                    ['PK_mscWarehouse', '=', $PK_mscWarehouse],
                ])
                ->get();

            $warehouse = $sqlsrv_warehouse->first();

            // Create warehouse
            $whs = new Warehouse;

            $whs->PK_mscWarehouse = $warehouse->PK_mscWarehouse;

            $whs->description = $warehouse->description;

            $whs->save();

            $data = $warehouse;

            DB::commit();

        } catch(\Exception $e) {
            
            DB::rollBack();

            throw $e;
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = [];

        DB::beginTransaction();

        try{

            // Validation
            $this->validate($request, [
                'PK_mscWarehouse' => 'required',
            ]);

            $PK_mscWarehouse = $request->PK_mscWarehouse;

            $sqlsrv_warehouse = SQLSRVWarehouse::select('PK_mscWarehouse', 'description')
                ->where([
                    ['PK_mscWarehouse', '=', $PK_mscWarehouse],
                ])
                ->get();

            $warehouse = $sqlsrv_warehouse->first();

            // Update mscWarehouse
            Warehouse::where('PK_mscWarehouse', $warehouse->PK_mscWarehouse)
                ->update(['description' => $warehouse->description]);

            $data = $warehouse;

            DB::commit();

        } catch(\Exception $e) {
            
            DB::rollBack();

            throw $e;
        }

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
