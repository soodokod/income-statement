<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Account;
use App\Warehouse;

use DB;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];

        $search = $request->input('search');

        $accounts = Account::select('PK_tempAcctDesc', 'FK_mscWarehouse', 'description')
            ->where('description', 'like', "%$search%")
            ->paginate(25);

        $data['accounts'] = $accounts;

        $warehouses = DB::table('mscwarehouse')
            ->select('PK_mscWarehouse as value', 'description as text')
            ->get();

        $data['warehouses'] = $warehouses;

        return view('account.home', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = [];

        DB::beginTransaction();

        try{

            $this->validate($request, [
                'name' => 'required',
                'value' => 'required',
            ]);

            $description = $request->name;

            $FK_mscWarehouse = $request->value;


            $account = new Account;

            $account->FK_mscWarehouse = $FK_mscWarehouse;

            $account->description = $description;

            $account->save();


            $data['success'] = true;

            DB::commit();

        } catch(\Exception $e) {
            
            DB::rollBack();

            throw $e;
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {        
        $data = [];

        DB::beginTransaction();

        try{

            $this->validate($request, [
                'pk' => 'required',
                'value' => 'required',
            ]);

            $PK_tempAcctDesc = $request->pk;

            $FK_mscWarehouse = $request->value;

            $account = Account::find($PK_tempAcctDesc);

            $account->FK_mscWarehouse = $FK_mscWarehouse;

            $account->save();

            $data['success'] = true;

            DB::commit();

        } catch(\Exception $e) {
            
            DB::rollBack();

            throw $e;
        }

        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
