<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use Illuminate\Pagination\LengthAwarePaginator;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = [];

        // Get Database name
        $database_name = DB::connection('mysql')->getDatabaseName();

        // Get the no. rows in tables
        $tables = DB::table('information_schema.TABLES')
            ->where('TABLE_SCHEMA', '=', $database_name)
            ->get()
            ->pluck('TABLE_ROWS', 'TABLE_NAME');

        $sprepfaglfsis = [];

        if ($tables->get('mscwarehouse') > 0 
            && $tables->get('repfaglfsis') > 0 
            && $tables->get('tempacctdesc') > 0) {
            
            $sprepfaglfsis = DB::select("CALL sprepfaglfsis");

        }

        $data['sprepfaglfsis_thead'] = array_keys((array) $sprepfaglfsis[0]);

        // Manually Create a new paginator instance
        $perPage = 42;

        $currentPage = $request->input('page', 1);

        $offSet = ($currentPage * $perPage) - $perPage;  

        $items = array_slice($sprepfaglfsis, $offSet, $perPage, true);  

        $total = count($sprepfaglfsis);

        $pagination = new LengthAwarePaginator($items, $total, $perPage, $currentPage);

        $path = route('report.home');

        // Set the base path to assign to all URLs
        $pagination->setPath($path);
        
        $data['sprepfaglfsis_tbody'] = $pagination;

        return view('report.home', $data);
    }

    /**
     * Download Report
     */
    public function download()
    {
        // Get Database name
        $database_name = DB::connection('mysql')->getDatabaseName();

        // Get the no. rows in tables
        $tables = DB::table('information_schema.TABLES')
            ->where('TABLE_SCHEMA', '=', $database_name)
            ->get()
            ->pluck('TABLE_ROWS', 'TABLE_NAME');

        if ($tables->get('mscwarehouse') > 0 
            && $tables->get('repfaglfsis') > 0 
            && $tables->get('tempacctdesc') > 0) {
            
            $sprepfaglfsis = DB::select("CALL sprepfaglfsis");

            // PhpSpreadsheet
            $spreadsheet = new Spreadsheet();

            // Setting a Font name and size
            $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');

            $spreadsheet->getDefaultStyle()->getFont()->setSize(12);

            $sheet = $spreadsheet->getActiveSheet();
            
            $row = 1;

            $column = 1;

            // Config Headers by getting the top of $sprepfaglfsis using current()
            $headers = array_keys((array) current($sprepfaglfsis));

            // Set bold Header
            $sheet->getStyle('1:1')->getFont()->setBold(true);

            // Freezing first line https://www.askingbox.com/question/phpexcel-freeze-first-line-and-column
            $sheet->freezePane('A2');

            for (; $column <= count($headers); $column++) { 
                
                $header = $headers[$column - 1];
                
                // Setting a cell value by column and row
                $sheet->setCellValueByColumnAndRow($column, $row, $header);

                // Setting a column's width
                $sheet->getColumnDimensionByColumn($column)->setAutoSize(true);

            }

            // Config Content
            foreach ($sprepfaglfsis as $key => $value) {

                ++$row;

                $repfaglfsis = collect($value);

                for ($column = 1; $column <= $repfaglfsis->count(); $column++) { 
                    
                    $key = $headers[$column - 1];

                    $value = $repfaglfsis->get($key);

                    $n = str_replace(',', '', $value);

                    $datatype = $column > 1 && is_numeric($n) ? DataType::TYPE_NUMERIC : DataType::TYPE_STRING;

                    switch ($datatype) {
                        
                        case DataType::TYPE_NUMERIC:

                            $value = $n;
                            
                            break;

                        default: break;

                    }

                    // Retrieving a cell by column and row, and Explicitly set a cell's datatype and value
                    $sheet->getCellByColumnAndRow($column, $row)->setValueExplicit($value,$datatype);

                    switch ($datatype) {

                        case DataType::TYPE_NUMERIC:

                            // Setting a Number Format
                            $sheet->getCellByColumnAndRow($column, $row)->getStyle()->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                            
                            break;

                        default: break;

                    }

                }
                
            }

            $writer = new Xlsx($spreadsheet);

            $filename = 'repfaGLFSIS.xlsx';

            $writer->save($filename);

            return redirect("/$filename");

        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
