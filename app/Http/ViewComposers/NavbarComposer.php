<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;

use App\SQLSRVWarehouse;
use App\Warehouse;
use DB;

class NavbarComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $data = [];

        /** Config MySQL Warehouse **/

        $warehouse = Warehouse::select('PK_mscWarehouse', 'description');

        /** Config SQL Server Warehouse **/

        // Not included in Department
        $coll = collect([
            1000, // Not Applicable
            1014, // Dept.Ancill.Ware 001
            1029, // Dept.Ancill.Ware 002
            1032, // Dept.Ancill.Ware 003
            1036  // Dept.Ancill.Ware 004
        ]);

        $keys = Warehouse::select('PK_mscWarehouse', 'description')
            ->get()
            ->pluck('PK_mscWarehouse');

        $keys = $coll->merge($keys);
        
        $sqlsrv_warehouse = SQLSRVWarehouse::select('PK_mscWarehouse', 'description')
            ->whereNotIn('PK_mscWarehouse', $keys);

        // Count Changed Warehouses and Unexported Department
        $whs = $warehouse->get();

        $whs = $whs->where('is_changed', true);

        $sqlsrv_whs = $sqlsrv_warehouse->get();

        /** Config Departments (Badges) **/

        $data['departments'] = $whs->count() + $sqlsrv_whs->count();

        /** Config repfaglfsis **/

        $repfaglfsis = DB::table('repfaglfsis as a')
            ->selectRaw('TRIM(a.TempAcctDesc) as TempAcctDesc, b.description')
            ->leftJoin('tempacctdesc as b', DB::raw('TRIM(a.TempAcctDesc)'), '=', 'b.description')
            ->whereNotIn('a.TempAccountType', ['D','E'])
            ->whereNotIn('a.TempClassType', ['S','H','T'])
            ->where('b.description', '=', null)
            ->get();

        /** Config Undefined Accounts (Badges) **/

        $data['undefined'] = $repfaglfsis->count();

        $view->with('badges', (object) $data);
    }
}