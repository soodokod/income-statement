<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SQLSRVWarehouse extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'sqlsrv_bizbox';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mscWarehouse';

    protected $primaryKey = 'PK_mscWarehouse';
}
