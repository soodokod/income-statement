<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mscwarehouse';

    protected $primaryKey = 'PK_mscWarehouse';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['PK_mscWarehouse', 'description'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['is_changed'];

    /**
     * Check if MySQL warehouse is changed in SQL Server
     *
     * @return bool
     */
    public function getIsChangedAttribute()
    {
        $PK_mscWarehouse = $this->attributes['PK_mscWarehouse'];

        $description = $this->attributes['description'];

        $sqlsrv_warehouse = SQLSRVWarehouse::find($PK_mscWarehouse);

        return $description != $sqlsrv_warehouse->description;
    }
}
