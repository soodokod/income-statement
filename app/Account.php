<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tempacctdesc';

    protected $primaryKey = 'PK_tempAcctDesc';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['FK_mscWarehouse', 'description'];
}
